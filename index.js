const express = require('express');
const http = require('http');
const dotenv = require('dotenv').config();

const morgan = require('morgan');
const app = express();
const router = require('./router');
const mongoose = require('mongoose');
const cors = require('cors');
const multer = require('multer');

// Map global promise and set strictQuery to get rid of warnings
mongoose.Promise = global.Promise;
mongoose.set('strictQuery', false);

// DB setup
const connectionString = process.env.MONGO_CONNECTION_STRING;
mongoose.connect(connectionString, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log('MongoDB Connected...'))
  .catch(err => console.log(err));

// App setup
app.use(morgan('combined'));
app.use(cors());
app.use(multer({dest:'./uploads/'}).single('image'));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Cache-Control, Key, Access-Control-Allow-Origin");
    next();
});

app.use(express.static('uploads'));
router(app);

// Server setup
const port = process.env.PORT || 3090;
const server = http.createServer(app);
server.listen(port);
console.log('The server is listening on port ', + port);