# Property Management Application Server
> The template is provided for the students of the [Bottega Code School](https://bottega.tech/) and is a *fork from [prop-management-server](https://github.com/bottega-code-school/prop-management-server)*.

This project represents the server back-end of the [property management application](https://gitlab.com/K_25565/property-Management).  The server handles login requests from the clients as well as keeps track of the application's newsletters and requests.

To set it up, you must do the following steps:
- cd into prop-management-server
- `npm install` or `yarn install`
- `touch .env`
- Put your secrets in .env
  ```
  SALT=Put_A_Random_Set_Of_Characters_Here
  MONGO_CONNECTION_STRING=Put_Your_Connection_String_Here
  ```
- `npm run dev` or `yarn run dev` to run the dev server
- `npm run prod` or `yarn run prod` to run the prod server

While this project was originally started in 2018 as part of the Bottega Code School, it has been in 2023 to fix bugs that were present in the original code.  These bugfixes were accomplished by myself with no help from outside sources besides library documentation and Google.