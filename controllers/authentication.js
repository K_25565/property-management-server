const User = require('../models/user');
const jwt = require('jwt-simple');
const dotenv = require('dotenv').config();

function tokenForUser(user) {
    const timestamp = new Date().getTime();
    return jwt.encode({ sub: user.id, iat: timestamp }, process.env.SALT);
}

exports.signin = (req, res, next) => {
    // use email and password already authenticated
    // just need to provide a session token now
    res.json({ 
        token: tokenForUser(req.user),
        user: req.user
      });
}

exports.signup = (req, res, next) => {
    const fullname = req.body.fullname;
    const email = req.body.email;
    const password = req.body.password;
    const unit = req.body.unit;
    const admin = req.body.admin ? req.body.admin : false;

    if(!email || !password) {
        res.status(422).send({ error: "You must supply an email and a password."})
    }

    User.findOne({ email: email }, (err, userFound) => {
        if(err) { return next(err); }

        if (userFound) {
            return res.status(422).send({ error: "The requested email is currently in use." });
        }

        const user = new User({
            fullname: fullname,
            unit: unit,
            email: email,
            password: password,
            admin: admin
        });

        user.save(err => {
            
          if(err) { return next(err); }  

          res.json({ 
              token: tokenForUser(user),
              user: user
            });
        });
    });
}

exports.forgotPassword = (req, res, next) => {
    // This code is insecure as you can change any account's password as long as you have the email.
    User.findOne({email: req.body.email}, (err, userFound) => {
        if (err) { 
            return next(err); 
        } 

        if (!userFound){
            return res.status(422).send({error: "The given email was not found!"});
        }

        const user = userFound;
        if (req.body.newPassword == req.body.newPasswordConfirm) {
            user.password = req.body.newPassword;
            user.save(err => {
                if (err) {
                    return next(err);
                }

                res.json({
                    token: tokenForUser(user),
                    user: user
                })
            })
        }
        else {
            return res.status(422).send({ error: "The new passwords don't match!"});
        }
    })
}