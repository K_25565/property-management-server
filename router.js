const Authentication = require('./controllers/authentication');
const passportService = require('./server/passport');
const passport = require('passport');

// Models
const Newsletter = require('./models/newsletter');
const Request = require('./models/request');
const User = require('./models/user');

const requireAuth = passport.authenticate('jwt', { session: false });
const requireSignin = passport.authenticate('local', { session: false })

const bodyParser = require('body-parser');
const useBodyParser = bodyParser.json({ type: '*/*'});

const ejs = require('ejs');
var templateString = null;
var fs = require('fs');
var templateString = fs.readFileSync('./views/docs.ejs', 'utf-8');

module.exports = (app) => {
    // General
    app.get('/', function(req, res) {

        res.send(ejs.render(templateString));
    })

    // Authorization
    app.post('/signin', useBodyParser, requireSignin, Authentication.signin);

    app.post('/signup', useBodyParser, Authentication.signup);

    app.post('/forgot-password', useBodyParser, Authentication.forgotPassword);

    // Newsletters
    app.get('/newsletters', function(req, res) {
        Newsletter.find({}, function(err, newsletters) {
            res.send(newsletters);
        })
    })

    app.post('/newsletters/edit/:id', function(req, res) {
        Newsletter.findOne({ _id: req.body._id }, (err, newsletter) => {
            newsletter.title = req.body.title;
            newsletter.body = req.body.body;

            if (req.file)
            {
                newsletter.imageUrl = req.file.filename;
            }
            
            newsletter.save();

            if(err) { return next(err); }
            res.send({
                newsletter,
                sucess: true,
                message: "Edited newsletter."
            })
        });
    })

    app.post('/newsletter/new', function(req,res){

        var newNewsletter = new Newsletter();
        newNewsletter.title = req.body.title;
        newNewsletter.body = req.body.body
        newNewsletter.date = new Date();
        newNewsletter.imageUrl = req.file.filename;
        newNewsletter.save();
        res.send({
            success: true,
            message: 'Saved newsletter to server.',
            // data: fs.readFileSync(target_path, 'base64')
        })
    });

    // Requests
    app.get('/requests', function(req, res) {
        Request.find({}, function(err, requests) {
            res.send(requests);
        })
    })

    app.post('/requests/update-status', useBodyParser, function(req, res) {
        const id = req.body._id;
        Request.findOne({ _id: id}, function(err, request) {
            if(err) { res.send(err) }

            switch(request.status) {
                case "pending":
                    request.status = "in-progress";
                    request.save();

                    res.send({
                        success: true,
                        message: "Changed request status from \"pending\" to \"in-progress\"."
                    })
                    break;

                case "in-progress":
                    request.status = "complete";
                    request.save();

                    res.send({
                        success: true,
                        message: "Changed request status from \"in-progress\" to \"complete\"."
                    })
                    break;

                case "complete":
                    request.status = "pending";
                    request.save();

                    res.send({
                        success: true,
                        message: "Changed request status from \"complete\" to \"pending\"."
                    })
                    break;

                default:
                    break;
            }
        })
    })

    app.post('/requests/new', function(req, res) {
        User.findOne({_id: req.body.postedBy}, (err, user) => {
            if(err) { res.send(err) }

            const title = req.body.title;
            const body = req.body.body;
            const date = new Date(); // creation date
            const imageUrl = req.file.filename;
            const postedBy = req.body.postedBy;
            const posterName = user.fullname;
            const posterUnit = user.unit;
            const status = 'pending'; // pending by default

            const request = new Request({
                title,
                body,
                date,
                imageUrl,
                postedBy,
                posterName,
                posterUnit,
                status,
            })

            request.save();
            
            res.send({
                success: true,
                message: "Saved request to server.",
            })
        })
    })
}